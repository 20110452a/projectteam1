package fpt.intern.fa_api.service.impl;

import fpt.intern.fa_api.exception.ApplicationException;
import fpt.intern.fa_api.exception.NotFoundException;
import fpt.intern.fa_api.exception.ValidationException;
import fpt.intern.fa_api.exception.NotFoundException;
import fpt.intern.fa_api.model.entity.ClassEnity;
import fpt.intern.fa_api.model.filter.ClassEntityFilter;
import fpt.intern.fa_api.model.filter.ClassEntitySpec;
import fpt.intern.fa_api.model.mapper.ClassMapper;
import fpt.intern.fa_api.model.request.ClassRequest;
import fpt.intern.fa_api.model.response.ClassEntityPageResponse;
import fpt.intern.fa_api.model.response.ClassResponse;
import fpt.intern.fa_api.repository.ClassRepository;
import fpt.intern.fa_api.service.ClassService;
import fpt.intern.fa_api.util.ValidatorUtil;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



@Service
@RequiredArgsConstructor
public class ClassServiceImpl implements ClassService {
    private final ClassRepository repository;
    private final ClassMapper classMapper;

    private final ValidatorUtil validatorUtil;


	@Override
	public List<ClassResponse> findAll() {
		try {
			List<ClassEnity> list = repository.findAll();
			List<ClassResponse> ListclassResponse = classMapper.toResponseList(list);

			return ListclassResponse;
		} catch (ApplicationException ex) {
			throw ex;
		}
	}

	@Override
	public List<ClassResponse> findClassWithSorting(String field){
		try {
			List<ClassEnity> list = repository.findAll(Sort.by(Sort.Direction.ASC,field));
			List<ClassResponse> ListclassResponse = classMapper.toResponseList(list);

			return ListclassResponse;
		} catch (ApplicationException ex) {
			throw ex;
		}
	}

	@Override
	public List<ClassResponse> findClassWithPagination(int page, int pagesize){
		try {
			Page<ClassEnity> classenitylist = repository.findAll(PageRequest.of(page, pagesize));
			List<ClassEnity> list = new ArrayList<ClassEnity>(classenitylist.getSize());
			for ( ClassEnity classEnity : classenitylist ) {
				list.add(classEnity);
			}
			List<ClassResponse> ListclassResponse = classMapper.toResponseList(list);
			return ListclassResponse;
		} catch (ApplicationException ex) {
			throw ex;
		}
	}


	@Override
	public List<ClassResponse> findClassWithPaginationAndSorting(int page, int pagesize, String field){
		try {
			Page<ClassEnity> classenitylist = repository.findAll(PageRequest.of(page, pagesize).withSort(Sort.by(field)));
			List<ClassEnity> list = new ArrayList<ClassEnity>(classenitylist.getSize());
			for ( ClassEnity classEnity : classenitylist ) {
				list.add(classEnity);
			}
			List<ClassResponse> ListclassResponse = classMapper.toResponseList(list);
			return ListclassResponse;
		} catch (ApplicationException ex) {
			throw ex;
		}
	}


	@Override
	public ClassResponse save(ClassRequest classRequest, BindingResult bindingResult) {
		try {


			if (bindingResult.hasErrors()) {
				Map<String, String> validationErrors = validatorUtil.toErrors(bindingResult.getFieldErrors());
				throw new ValidationException(validationErrors);
			}

			// Map to Entity
			ClassEnity classEntity = classMapper.toEntity(classRequest);

			// Save
			repository.saveAndFlush(classEntity);

            // Map to Response
            return classMapper.toResponse(classEntity);
        } catch (ApplicationException ex) {
            throw ex;
        }
    }


		@Override
		public ClassResponse update(ClassRequest classRequest, BindingResult bindingResult) {
			 try {
		            Optional<ClassEnity> existingClass = findByID(classRequest.getId());
		            if (existingClass == null) {
		                throw new NotFoundException("Product Not Found");
		            }


		            if (bindingResult.hasErrors()) {
		                Map<String, String> validationErrors = validatorUtil.toErrors(bindingResult.getFieldErrors());
		                throw new ValidationException(validationErrors);
		            }

		            // Map to Entity
		            ClassEnity classEntity = classMapper.toEntity(classRequest);


		            // Update
		            repository.saveAndFlush(classEntity);

		            // Map to Response
		            return classMapper.toResponse(classEntity);
		        } catch (ApplicationException ex) {
		            throw ex;
		        }
		}


		@Override
		public Optional<ClassEnity> findByID(long id) {

			return repository.findById(id);
		}

    @Override
    public List<ClassResponse> findByCodeAndName(String code, String name) {
        List<ClassResponse> responses = new ArrayList<>();
        repository.findByCodeAndName(code, name).forEach(x -> {
            responses.add(classMapper.toResponse(x));
        });
        if (responses.isEmpty()) throw new NotFoundException("Không bản ghi nào được tìm thấy");
        return responses;
//            return repository.findByCodeAndName(code,name);
    }

    @Override
    public ClassEntityPageResponse<ClassResponse> searchClass(ClassEntityFilter filter, int page, int size) {
        Specification<ClassEnity> spec = ClassEntitySpec.filterBy(filter);
        Page<ClassEnity> pageResult = repository.findAll(spec, PageRequest.of(page, size));
        return classMapper.toClassEntityPageResponse(pageResult);
    }
}



