package fpt.intern.fa_api.service;

import java.util.Optional;

import org.springframework.validation.BindingResult;

import fpt.intern.fa_api.model.entity.ClassEnity;
import fpt.intern.fa_api.model.filter.ClassEntityFilter;
import fpt.intern.fa_api.model.request.ClassRequest;
import fpt.intern.fa_api.model.response.ClassEntityPageResponse;
import fpt.intern.fa_api.model.response.ClassResponse;

import java.util.List;

import java.util.List;

public interface ClassService {

	List<ClassResponse> findAll();
	List<ClassResponse> findClassWithSorting(String field);
	List<ClassResponse> findClassWithPagination(int page, int limit);
	List<ClassResponse> findClassWithPaginationAndSorting(int page, int limit, String field);
	ClassResponse save(ClassRequest classRequest, BindingResult bindingResult);
	Optional<ClassEnity> findByID(long id);
	ClassResponse update(ClassRequest classRequest, BindingResult bindingResult);
	List<ClassResponse> findByCodeAndName(String code, String name);

    ClassEntityPageResponse<ClassResponse> searchClass(ClassEntityFilter filter, int page, int size);
}
