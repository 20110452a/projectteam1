package fpt.intern.fa_api.repository;

import fpt.intern.fa_api.model.entity.ClassEnity;
import jakarta.annotation.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassRepository extends JpaRepository<ClassEnity, Long>, JpaSpecificationExecutor<ClassEnity> {
    @Query(value = "Select * from class where class_id='1'", nativeQuery = true)
    public ClassEnity checkliked();
    List<ClassEnity> findByCodeAndName(String code, String name);
	List<ClassEnity> searchAllByIdAndName(Integer id, String name);
	@Query(value = "SELECT count(code) FROM Class WHERE code LIKE CONCAT('%', :year, '%')", nativeQuery = true)
	public int findLastClassCodeByYear(@Param("year")String year);
}
