package fpt.intern.fa_api.model.filter;

import fpt.intern.fa_api.model.entity.ClassEnity;
import fpt.intern.fa_api.model.enums.ClassStatus;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
public class ClassEntitySpec {
    public static final String LOCATIONS = "location";
    public static final String DATE_FROM = "startDate";

    public static final String DATE_TO = "endDate";
    public static final String TIME = "time";
    public static final String CLASS_STATUS = "status";
    public static final String FSU = "fsu";

    private ClassEntitySpec() {
    }

    public static Specification<ClassEnity> filterBy(ClassEntityFilter classEntityFilter) {
        return Specification
                .where(hasLocations(classEntityFilter.locations()))
                .and(hasClassStatus(classEntityFilter.classStatus()))
                .and(hasFsu(classEntityFilter.fsu()))
                .and(hasDateGreaterThan(classEntityFilter.dateTo()))
                .and(hasDateLessThan(classEntityFilter.dateFrom()))
                .and(hasTimeGreaterThan(classEntityFilter.timeFrom()))
                .and(hasTimeLessThan(classEntityFilter.timeTo()));
    }

    private static Specification<ClassEnity> hasLocations(List<String> locations) {
        return (root, query, criteriaBuilder) -> {
            if (locations == null || locations.isEmpty()) {
                return criteriaBuilder.conjunction();
            } else {
                return criteriaBuilder.or(locations.stream()
                        .map(location -> criteriaBuilder.equal(root.get(LOCATIONS), location))
                        .toArray(Predicate[]::new));
            }
        };
    }

    private static Specification<ClassEnity> hasClassStatus(ClassStatus classStatus) {
        return (root, query, cb) -> classStatus == null ? cb.conjunction() : cb.equal(root.get(CLASS_STATUS), classStatus);
    }

    private static Specification<ClassEnity> hasDateGreaterThan(LocalDate dateTo) {
        return (root, query, cb) -> dateTo == null ? cb.conjunction() : cb.greaterThanOrEqualTo(root.get(DATE_TO), dateTo);
    }

    private static Specification<ClassEnity> hasDateLessThan(LocalDate dateFrom) {
        return (root, query, cb) -> dateFrom == null ? cb.conjunction() : cb.lessThanOrEqualTo(root.get(DATE_FROM), dateFrom);
    }

    private static Specification<ClassEnity> hasTimeGreaterThan(LocalTime timeFrom) {
        return (root, query, cb) -> timeFrom == null ? cb.conjunction() : cb.greaterThanOrEqualTo(root.get(TIME), timeFrom);
    }

    private static Specification<ClassEnity> hasTimeLessThan(LocalTime timeTo) {
        return (root, query, cb) -> timeTo == null ? cb.conjunction() : cb.lessThanOrEqualTo(root.get(TIME), timeTo);
    }

    private static Specification<ClassEnity> hasFsu(String fsu) {
        return (root, query, cb) -> fsu == null || fsu.isEmpty() ? cb.conjunction() : cb.equal(root.get(FSU), fsu);
    }
}
