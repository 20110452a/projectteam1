package fpt.intern.fa_api.model.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name ="Permission")
public class Permission {
   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "permission_id")
	private int id;
	 
	@NotBlank(message = "Name may not be blank")
	@Column(nullable = false, name = "name")
	private String name;  

	@Column(name = "description")
	private String description;

	@Size(min = 1, max = 1)
	@Column(name = "status")
	private String status;

	@Column(name = "created_date")
	private Date created_date;

	@Column(name = "modified_date")
	private Date modified_date;

	@ManyToMany
	@JoinTable(
	    name = "Group_roles_permission",
	    joinColumns = @JoinColumn(name = "permission_id"),
	    inverseJoinColumns = @JoinColumn(name = "roles_id")
	)
	private List<Roles> roles;
}
