package fpt.intern.fa_api.model.entity;

import fpt.intern.fa_api.model.enums.SyllabusStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "syllabus")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Syllabus {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String technicalGroup;
    private String version;
    private String trainingAudience;
    private String topicOutline;
    private String trainingMaterials;
    private String trainingPrinciples;
    private String priority;
    @Enumerated(EnumType.STRING)
    private SyllabusStatus status;

    @OneToOne
    @JoinColumn(name = "userCreate", referencedColumnName = "id")
    private User createdBy;

    @ManyToMany(mappedBy = "syllabusList")
    private List<Training> trainingList;
}
