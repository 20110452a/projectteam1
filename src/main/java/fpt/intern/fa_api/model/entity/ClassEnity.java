package fpt.intern.fa_api.model.entity;

import fpt.intern.fa_api.model.enums.ClassStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "Class")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClassEnity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private String code;

    @Enumerated(EnumType.STRING)
    private ClassStatus status;

    private String location;

    private String fsu;

    private Date startDate;

    private Date endDate;

    @OneToOne
    @JoinColumn(name = "userCreate", referencedColumnName = "id")
    ////////////
    private User createdBy;

    private Date createdDate;

    @OneToOne
    @JoinColumn(name = "userModify", referencedColumnName = "id")
    private User modifiedBy;

    private Date modifiedDate;

    @ManyToMany(mappedBy = "classEnityList")
    private List<User> userList;
//    @ManyToMany
//    @JoinTable(
//            name = "Group_class_training",
//            joinColumns = @JoinColumn(name = "class_id"),
//            inverseJoinColumns = @JoinColumn(name = "training_id")
//    )
//    private List<Training> listTraining;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;



    
    

}
