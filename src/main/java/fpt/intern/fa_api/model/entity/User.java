package fpt.intern.fa_api.model.entity;

import java.sql.Date;
import java.util.List;

import fpt.intern.fa_api.model.enums.Gender;
import fpt.intern.fa_api.model.enums.UserStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name ="User")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name may not be blank")
    @Column(nullable = false,name="username")
    private String username;

    @Enumerated(EnumType.STRING)
    private Gender gender;
    
    @Column(unique=true)
    private String phone;

    private Date dateOfBirth;

    private String address;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    private String email;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roles_id")
    private Roles Roles;
   

    @Size(min=8,max= 16, message = "Please enter password 8-16 character")
    @Column(name = "password")
    private String password;
    
    @ManyToMany
    @JoinTable(
        name = "Group_user_class",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "class_id")
    )
    private List<ClassEnity> classEnityList;
}
