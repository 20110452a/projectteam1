package fpt.intern.fa_api.model.entity;

import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fpt.intern.fa_api.model.entity.enums.TrainingStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name ="Training")
@Data
public class Training {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name may not be blank")
    @Column(nullable = false)
    private String name;

    private String description;

    private String duration;

    private TrainingStatus status;

    private Date startDate;
    
    private Date endDate;

    private int createdBy;

    private Date createdDate;

    private int modifiedBy;

    private Date modifiedDate;
//    @ManyToMany(mappedBy = "listTraining")
//    private List<ClassEnity> classEnityList;

    @ManyToMany
    @JoinTable(
            name = "training_syllabus",
            joinColumns = @JoinColumn(name = "training_id"),
            inverseJoinColumns = @JoinColumn(name = "syllabus_id")
    )
    private List<Syllabus> syllabusList;

//    @ManyToMany(mappedBy = "Lessons")
//    private List<Lessons> listLessons;
}
