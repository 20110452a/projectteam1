package fpt.intern.fa_api.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import fpt.intern.fa_api.model.entity.User;
import jakarta.persistence.Column;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

@Setter
@Getter
public class ClassResponse implements Serializable {

		private int id;

	    private String name;
	   
	    private String code;
	    
	    private String Duration;
	  
	    private String status;
	 
	    private String Location;

	    private String FSU;

	    private Date startdate;
	
	    private Date enddate;
	    
	    private int created_by;

	    private Date created_date;
	    
	    private int modified_by;

	    private Date modified_date;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getDuration() {
			return Duration;
		}

		public void setDuration(String duration) {
			Duration = duration;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getLocation() {
			return Location;
		}

		public void setLocation(String location) {
			Location = location;
		}

		public String getFSU() {
			return FSU;
		}

		public void setFSU(String fSU) {
			FSU = fSU;
		}

		public Date getStartdate() {
			return startdate;
		}

		public void setStartdate(Date startdate) {
			this.startdate = startdate;
		}

		public Date getEnddate() {
			return enddate;
		}

		public void setEnddate(Date enddate) {
			this.enddate = enddate;
		}

		public int getCreated_by() {
			return created_by;
		}

		public void setCreated_by(int created_by) {
			this.created_by = created_by;
		}

		public Date getCreated_date() {
			return created_date;
		}

		public void setCreated_date(Date created_date) {
			this.created_date = created_date;
		}

		public int getModified_by() {
			return modified_by;
		}

		public void setModified_by(int modified_by) {
			this.modified_by = modified_by;
		}

		public Date getModified_date() {
			return modified_date;
		}

		public void setModified_date(Date modified_date) {
			this.modified_date = modified_date;
		}

		public ClassResponse() {
			super();
		}
}
